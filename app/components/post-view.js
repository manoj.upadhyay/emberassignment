import Component from '@ember/component';
import { inject } from '@ember/service';

export default Component.extend({
  router:inject(),
  actions: {
    seePost(post) {
      this.get('router').transitionTo('posts.details', post.id)
    }
  }
});
