import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    return({
      post: this.store.findRecord('posts', params.id),
      comments: this.store.query('comments', {postId: params.id})       
    })
  },
  setupController(controller, model) {
    this._super(...arguments);
    controller.set('post', model.post);
    controller.set('comments', model.comments);
  },
  actions: {
    close() {
      window.history.back();
    }
  }
});
