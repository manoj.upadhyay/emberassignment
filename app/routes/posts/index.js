import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return this.store.findAll('posts')
  },
  actions: {
    seePost(post) {
      this.get('router').transitionTo('posts.details', post.id)
    }
  }
});
