import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return this.store.findAll('albums')
  },
  actions: {
    seeAlbum(album) {
      this.get('router').transitionTo('albums.details', album.id)
    }
  }
});
