import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    return ({
      album: this.store.findRecord('albums', params.id),
      photos: this.store.query('photos', {albumId: params.id})       
    })
  },
  setupController(controller, model) {
    this._super(...arguments);
    controller.set('album', model.album);
    controller.set('photos', model.photos);
  },
  actions: {
    showImage(photo) {
      this.get('router').transitionTo('albums.image-view', photo.id)
    },
    close() {
      window.history.back();

    }
  }
 });
