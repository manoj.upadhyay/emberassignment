import DS from 'ember-data';
import { underscore } from '@ember/string'; 
import { typeOf } from '@ember/utils';

export default DS.JSONSerializer.extend({
 keyForRelationship: function(attr) {
   return underscore(attr) + (typeOf(attr) === 'string' && (attr.endsWith('Id')? '': '_id'));
 },
});