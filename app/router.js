import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('posts', function() {
  this.route('details', {path: '/:id'});
  });
  this.route('albums', function() {
    this.route('details', {path: 'details/:id'});
    this.route('image-view', {path: 'image/:id'});
  });
});

export default Router;
