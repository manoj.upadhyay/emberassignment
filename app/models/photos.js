import DS from 'ember-data';

export default DS.Model.extend({
  "albumId": DS.belongsTo('albums'),
  "title": DS.attr('string'),
  "url": DS.attr('string'),
  "thumbnailUrl": DS.attr('string')
});
