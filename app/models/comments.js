import DS from 'ember-data';

export default DS.Model.extend({
  "post": DS.belongsTo('posts'),
  "name": DS.attr('string'),
  "email": DS.attr('string'),
  "body": DS.attr('string')
});
